const app = require('./app')
const http = require('http')
const process = require('process')

http.createServer(app)

const PORT = process.env.PORT || 3003
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})
